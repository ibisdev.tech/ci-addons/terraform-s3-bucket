variable "REGION" {
  type        = string
  description = "AWS Region"
}

variable "PROJECT_NAME" {
  type        = string
  description = "Project name"
}

variable "REPOSITORY_NAME" {
  type        = string
  description = "Repository name"
}

variable "ENVIRONMENT" {
  type        = string
  description = "Environment"
}
