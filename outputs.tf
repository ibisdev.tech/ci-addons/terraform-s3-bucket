output "iam_access_key_id" {
  value = aws_iam_access_key.access_key.id
}

output "iam_secret_access_key" {
  value     = aws_iam_access_key.access_key.secret
  sensitive = true
}

output "s3_bucket_id" {
  value = module.s3_bucket.s3_bucket_id
}

output "cloudfront_distribution_domain_name" {
  value = module.cloudfront_distribution.cloudfront_distribution_domain_name
}
