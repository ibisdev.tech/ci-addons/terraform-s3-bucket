locals {
  fullname = substr("${var.PROJECT_NAME}-${var.REPOSITORY_NAME}-${var.ENVIRONMENT}", 0, 36)
}
