provider "aws" {
  region = var.REGION

  skip_metadata_api_check     = true
  skip_region_validation      = true
  skip_credentials_validation = true
  skip_requesting_account_id  = true

  default_tags {
    tags = {
      Terraform   = "true"
      Project     = var.PROJECT_NAME
      Environment = var.ENVIRONMENT
    }
  }
}
