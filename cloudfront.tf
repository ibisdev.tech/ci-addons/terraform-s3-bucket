module "cloudfront_distribution" {
  source = "terraform-aws-modules/cloudfront/aws"

  comment             = module.s3_bucket.s3_bucket_id
  price_class         = "PriceClass_All"
  wait_for_deployment = false

  create_origin_access_control = true
  origin_access_control = {
    (local.fullname) : {
      "description" : local.fullname,
      "origin_type" : "s3",
      "signing_behavior" : "always",
      "signing_protocol" : "sigv4"
    }
  }

  origin = {
    s3_bucket = {
      domain_name           = module.s3_bucket.s3_bucket_bucket_regional_domain_name
      origin_access_control = local.fullname
    }
  }

  default_cache_behavior = {
    target_origin_id       = "s3_bucket"
    viewer_protocol_policy = "redirect-to-https"
    use_forwarded_values   = false

    cache_policy_id            = "658327ea-f89d-4fab-a63d-7e88639e58f6"
    origin_request_policy_id   = "88a5eaf4-2fd4-4709-b370-b4c650ea3fcf"
    response_headers_policy_id = "5cc3b908-e619-4b99-88e5-2cf7f45965bd"

    allowed_methods = ["GET", "HEAD", "OPTIONS"]
    cached_methods  = ["GET", "HEAD"]
    compress        = true
  }

  viewer_certificate = {
    cloudfront_default_certificate = true
  }
}
